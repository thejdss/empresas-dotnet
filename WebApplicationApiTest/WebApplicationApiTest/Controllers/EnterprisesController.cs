﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplicationApiTest.Data;

namespace WebApplicationApiTest.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly EnterpriseContext _context;

        public EnterprisesController(EnterpriseContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> EnterpriseIndex([FromRoute] int id)
        {
            var search = await _context.Enterprise.Include(e => e.enterprise_type).FirstOrDefaultAsync(i => i.id == id);

            if (search == null)
                return NotFound();

            return Ok(search);
        }

        [HttpGet]
        public async Task<ActionResult> GetEnterprises([FromQuery] int enterprise_types, [FromQuery] string name)
        {
            if (!String.IsNullOrEmpty(name) && enterprise_types >= 0)
            {
                var ent = from e in _context.Enterprise.Include(b => b.enterprise_type) select e;

                ent = ent.Where(a => a.enterprise_name.Contains(name) && a.enterprise_type.id == enterprise_types);
                await ent.ToListAsync();

                if (ent.Count() > 0)
                    return Ok(ent);
                else
                    return NotFound();
            }
            else
            {
                return Ok(_context.Enterprise.Include(e => e.enterprise_type));
            }
        }
    }
}