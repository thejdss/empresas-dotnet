﻿using Microsoft.EntityFrameworkCore;
using WebApplicationApiTest.Models;

namespace WebApplicationApiTest.Data
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
    }
}
