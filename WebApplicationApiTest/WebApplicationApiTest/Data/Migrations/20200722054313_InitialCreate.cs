﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplicationApiTest.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnterpriseType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprise_type_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Enterprise",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprise_name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    email_enterprise = table.Column<string>(nullable: true),
                    facebook = table.Column<string>(nullable: true),
                    twitter = table.Column<string>(nullable: true),
                    linkedin = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    own_enterprise = table.Column<bool>(nullable: false),
                    photo = table.Column<string>(nullable: true),
                    value = table.Column<int>(nullable: false),
                    shares = table.Column<int>(nullable: false),
                    share_price = table.Column<double>(nullable: false),
                    own_shares = table.Column<int>(nullable: false),
                    city = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true),
                    enterprise_typeid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprise", x => x.id);
                    table.ForeignKey(
                        name: "FK_Enterprise_EnterpriseType_enterprise_typeid",
                        column: x => x.enterprise_typeid,
                        principalTable: "EnterpriseType",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enterprise_enterprise_typeid",
                table: "Enterprise",
                column: "enterprise_typeid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enterprise");

            migrationBuilder.DropTable(
                name: "EnterpriseType");
        }
    }
}
