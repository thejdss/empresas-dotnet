﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationApiTest.Models;

namespace WebApplicationApiTest.Data
{
    public class InitializeData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<EnterpriseContext>();
                context.Database.EnsureCreated();
                
                if (context.Enterprise != null && context.Enterprise.Any())
                    return;

                var enterprises = GetEnterprises(context).ToArray();
                context.Enterprise.AddRange(enterprises);
                context.SaveChanges();
            }
        }
        public static List<Enterprise> GetEnterprises(EnterpriseContext db)
        {
            List<Enterprise> ent = new List<Enterprise>() {
                new Enterprise
                {
                    enterprise_name = "AllRide",
                    description = "Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry",
                    own_enterprise = false,
                    shares = 100,
                    share_price = 10000.0,
                    city = "Santiago",
                    country = "Chile",
                    enterprise_type = GetType(db, "Software")
                },
                new Enterprise
                {
                    enterprise_name = "AnewLytics SpA",
                    description = "We have one passion: to create value for our customers by analyzing the conversations their customers have with the Contact Center in order to extract valuable and timely information to understand and meet their needs. That´s how AnewLytics was born: a cloud-based analytics service platform that performs 100% automated analysis.",
                    own_enterprise = false,
                    shares = 100,
                    share_price = 10000.0,
                    city = "Santiago",
                    country = "Chile",
                    enterprise_type = GetType(db, "Service")
                },
                new Enterprise
                {
                    enterprise_name = "AQM S.A.",
                    description = "Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather.",
                    own_enterprise = false,
                    shares = 100,
                    share_price = 10000.0,
                    city = "Santiago",
                    country = "Chile",
                    enterprise_type = GetType(db, "Agro")
                }
            };
            return ent;
        }

        private static EnterpriseType GetType(EnterpriseContext db, string typeName)
        {
            var search = db.EnterpriseType.FirstOrDefault(i => i.enterprise_type_name == typeName);

            if (search == null)
                return new EnterpriseType { enterprise_type_name = typeName };
            else
                return search;
        }
    }
}
